#include <iostream>

class complex {
    public:
        complex(int r = 0, int i = 0);
        complex(const complex& c);
        
        int real() const;
        int imag() const;
        
        complex operator+=(const complex& c);
        
        void setReal(int r);
        void setImag(int i);
        
    private:
        int re, im;
};


complex::complex(int r, int i) {
    std::cout << "complex::complex(" << r << ", " << i << ")" << std::endl;    
    re = r;
    im = i;
}

complex::complex(const complex& c) {
    std::cout << "complex::copy(" << c.re << ", " << c.im << ")" << std::endl;
    re = c.re;
    im = c.im;
}

inline int complex::real() const {
    return re;    
}

inline int complex::imag() const {
    return im;
}

complex complex::operator+=(const complex& c) {
    re += c.real();
    im += c.imag();
    return *this; 
}

inline void complex::setReal(int r) {
    re = r;
}

inline void complex::setImag(int i) {
    im = i;
}

complex operator+(const complex& left, const complex& right) {
    return complex{left}+=(right);
}

bool operator==(const complex& left, const complex& right) {
    return left.real()==right.real() && left.imag()==right.imag();
}

class TestBase {        
    protected:
        void assert(bool expression, const char* message);
        
    public:
        virtual void runAllTests();
};


void TestBase::assert(bool expression, const char* message) {
    if (!expression) {
        std::cout << "Assertion failed: " << message << std::endl;
    }
}

void TestBase::runAllTests() {
}


class Lab7Tests : public TestBase {

    void testComplexClass1();
    
    void testComplexClass2();
    
    void testComplexClass3();    
    
    void testComplexClass4();    
    
    void testComplexClass5();
    
    void testComplexClass6();

    void f(complex c);
    
    void g(complex& c);
    
    public:
        void runAllTests() override;
};

void Lab7Tests::testComplexClass1() {
    complex c(1, 2);
 
    assert(c.real()==1, "Real part mismatch");
    assert(c.imag()==2, "Imag part mismatch");
}

void Lab7Tests::testComplexClass2() {
    complex c1(1, 2), c2(2, 3);

    assert((c1+c2).real()==3, "Addition: real part mismatch");
    assert(c1.real()==1, "Addition: operand has been modified");
    assert((c2+c1).imag()==5, "Addition: imag part mismatch");
    assert(c2.real()==2, "Addition: operand has been modified");
}

void Lab7Tests::testComplexClass3() {
    complex c(1, 2);

    assert((c+2).real()==3, "Scalar addition: real part mismatch");
    assert((c+2).imag()==2, "Scalar addition: imag part mismatch");
    assert((4+c).real()==5, "Scalar addition: real part mismatch");
    assert((4+c).imag()==2, "Scalar addition: imag part mismatch");
    assert(c.real()==1, "Scalar addition:operand has been changed");
    assert(c.imag()==2, "Scalar addition:operand has been changed");
}

void Lab7Tests::testComplexClass4() {
    complex c1, c2{2}, c3{3,4}, c4{0, 1}, c5=c2;

    assert(c1.real()==0 && c1.imag()==0, "c1 mis-initialized.");
    assert(c2.real()==2 && c2.imag()==0, "c2 mis-initialized.");
    assert(c3.real()==3 && c3.imag()==4, "c3 mis-initialized.");
    assert(c4.real()==0 && c4.imag()==1, "c4 mis-initialized.");
    assert(c5.real()==2 && c5.imag()==0, "c5 mis-initialized.");
}

void Lab7Tests::testComplexClass6() {
    complex a{3, 4}, b{2, 1}, c{7, 8};
    
    assert(a+b==b+a, "addition failed");
    assert((a+b)+c==a+(b+c), "addition failed");
    assert(a+0==a, "addition failed");
    assert(0+a==a, "addition failed");
}

void Lab7Tests::f(complex c) {
    std::cout << "Aha... copy ctor has been called." << std::endl;
    c.setReal(-1);
}
    
void Lab7Tests::g(complex& c) {
     std::cout << "Oohoo.. no ctor has been called." << std::endl;
     c.setReal(-1);
}
    
void Lab7Tests::testComplexClass5() {
    complex c{3, 4};
    f(c);
    assert(c.real()==3 && c.imag()==4, "c has been modified.");
    g(c);
    assert(c.real()==-1 && c.imag()==4, "c not been modified.");
}

void Lab7Tests::runAllTests() {
    testComplexClass1();
    testComplexClass2();
    testComplexClass3();
    testComplexClass4();
    testComplexClass5();
    std::cout << "All good";
}
    
int main() {
    Lab7Tests l2tests;
    l2tests.runAllTests();
    return 0;
}


