#include <iostream>
#include <sstream>
#include <list>
#include <string.h>

using namespace std;

class str {
public:
    str(const char* str = nullptr);
    
    str(const str& source);

    // accessor function
    const char* get () const;
    
    // assignment
    str& operator=(const str& right);
    str& operator+=(const str& right);
    
    //mutator
    str& set(int pos, char c);

    ~str();
    
private:
    char* s;
    
    char* set(const char* ccp);
    static int length(const char* ccp);
};


str::str(const char* str) : s {set(str)} {}
    
str::str(const str &source) : str {source.s} {}
    
inline const char* str::get () const {
    return (const char*)s;
}
    
str& str::operator=(const str& right) {
    // self-assignemt
    if (this == &right) {
        return *this;
    }
        
    if (s!=nullptr) {
		delete [] s;
	}
		
    s = set(right.s);
        
	return *this;
}

str& str::operator+=(const str& right) {
    char* olds = s;
    
    s = new char[length(olds) + length(right.s) + 1];
    
    if (olds!=nullptr) {
        strcpy(s, olds);
    }
    
    if (right.s!=nullptr) {
        strcat(s, right.s);
    }
    
    if (olds!=nullptr) {
        delete [] olds;
    }
    
    return *this;
}
    
str::~str() { 
    if (s!=nullptr) {
		delete [] s;
	}
}
    
char* str::set(const char* ccp) {
    s = nullptr;
	
    if (ccp!=nullptr) {
        s = new char [strlen(ccp) + 1];
        strcpy(s, ccp);
    }
        
	return s;
}

str& str::set(int pos, char c) {
    s[pos] = c;
    return *this;
}


int str::length(const char* ccp) {
    return ccp==nullptr ? 0 : strlen(ccp);
}

ostream& operator<<(ostream& os, const str& s) {
    os << s.get();
    return os;
}

str operator+(const str& left, const str& right) {
    return str{left}+=(right);
}

class Context { } ;

class Shape {
private:
    int id;
    char _rep[128];
    std::stringstream stream;
    
    static int ID;
    static int generateId();
    
protected:
    str repr() const;
    ostream& reprStream() ;
    
public:
    void display() const;
    Shape();
    Shape(const Shape& s);
	virtual void draw(const Context& context) = 0;
};

class Point : public Shape {
    int x, y;
public:
    Point(int a = 0, int b = 0);
    str toString() ;
	void draw(const Context& context) override;
};

class Line : /*protected*/public Shape {
    Point start, end;
public:
    Line();
    Line(const Point& p1, const Point& p2);
    str toString() ;
	void draw(const Context& context) override;
};

class Circle : public Shape {
    int radius;
    Point center;
public:
    Circle();
    Circle(const Point& c, int radius = 1);
    str toString() ;
	void draw(const Context& context) override;
};

int Shape::ID = 1000;

int Shape::generateId() {
    return Shape::ID++;
}

Shape::Shape(const Shape& s) 
    : id{s.id} {
    stream.rdbuf()->pubsetbuf(_rep, sizeof(_rep));
    strcpy(_rep, s._rep);
    cout << "Shape::Shape(const Shape&)" << endl;
}

Shape::Shape() : id{Shape::generateId()} {
    stream.rdbuf()->pubsetbuf(_rep, sizeof(_rep));
    cout << "Shape::Shape()" << endl;
}

str Shape::repr() const {
    return str{_rep};
}

inline ostream& Shape::reprStream() {
    stream.seekp(0);
    return stream;
}

void Shape::display() const {
    std::cout << "ID: " << id << " " << repr() << std::endl;
}

Point::Point(int a, int b) 
    : x{a}, y{b} {
    cout << "Point::Point(" << x << "," << y << ")" << endl;
}
    
str Point::toString() {
    //id = 100; // id not accessible because it is private in the base class Shape
    //Note that repr() and reprStream() are accessible because they are protected in the base class Shape
    reprStream() << "{type: Point, x: " << x << ", y: " << y << "}" << (char)0;
    return repr();
}

void Point::draw(const Context& context) {
	std::cout << toString();
}

Line::Line() {
    cout << "Line::Line()" << endl;
}

Line::Line(const Point& p1, const Point& p2) : start{p1}, end{p2} {
    cout << "Line::Line(" << start.toString() << "," << end.toString() << ")" << endl;
}

str Line::toString() {
    reprStream() << "{type: Line, from: " << start.toString() << ", to: " << end.toString() << "}" << (char)0;
    return repr();
}

void Line::draw(const Context& context) {
	std::cout << toString();
}

Circle::Circle() {
    cout << "Circle::Circle()" << endl; 
}

Circle::Circle(const Point& c, int r) : center{c}, radius{r}{
    cout << "Circle::Circle(" << center.toString() << "," << radius << ")" << endl;
}


str Circle::toString() {
    reprStream() << "{type: Circle, center: " << center.toString() << ", radius: " << radius << "}" << (char)0;
    return repr();
}

void Circle::draw(const Context& context) {
	std::cout << toString();
}


void testLab9() {	
	Context context;
   	std::list<Shape*> shapes;
	Point p;
	Circle c;
	Line l;
	Shape* ps = new Point(1, 2);
	
	shapes.push_back(&p);
	shapes.push_back(&c);
	shapes.push_back(&l);
	shapes.push_back(ps);
	
	for(list<Shape*>::iterator i = shapes.begin(); i != shapes.end(); i++) {
	    (*i)->draw(context);
	}
}

int main() {
	testLab9();
	
	return 0;
}
