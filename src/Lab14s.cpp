#include <string.h>
#include <string>
#include <iostream>
#include <sstream>
#include <typeinfo>

using namespace std;

void assert(bool expression, const char* message) {
    if (!expression) {
        cout << "Assertion failed: " << message << endl;
    }
}

// Date.h
class Date {
public: // public interface:
    Date(int day, int month, int year);

     // non-modifying functions for examining the Date:
     int day() const;
     int month() const;
     int year() const;
     const char* toString() const; // C-style string representation
     bool compare(const Date& other) const;

private:
     int _day, _month, _year; // representation
     char _strRepresentation[11]; // C-style string representation
};

// Date.cpp
Date::Date(int day, int month, int year) 
    : _day{day}, _month{month}, _year{year} {
	// build C-style string representation
    std::stringstream stream;
    stream.rdbuf()->pubsetbuf(_strRepresentation, sizeof(_strRepresentation));
    stream << _day << "/" << _month << "/" << year << (char)0;
}

inline int Date::day() const {
     return _day;
}
 
inline int Date::month() const {
     return _month;
}
 
inline int Date::year() const {
     return _year;
}
 
inline const char* Date::toString() const { 
    return _strRepresentation;
}

inline bool Date::compare(const Date& other) const {
    return _day==other._day && _month==other._month && _year==other._year;
}
 
 
// main.cpp
class IMyObject {
public:
    virtual IMyObject* clone() const = 0;
    virtual string toString() const = 0;
};


class UniMember : public IMyObject {
   string _name;
   Date _dob;

public:
   UniMember(const char* n, const Date& d);  
   string toString() const final override;
   
   const char* name() const { return _name.c_str(); }
   const Date& dob() const { return _dob; }
};

UniMember::UniMember(const char* n, const Date& d)
    : _name{n}, _dob{d} {
}

string UniMember::toString() const {
    return "UniMember(" + _name + ", " + _dob.toString() + ")";
}

class Student : public virtual UniMember {

    Student(const Student& r) : Student{r.name(), r.dob()}{
        
    }
    
public:
    Student(const char* n, const Date& d)
        : UniMember(n, d) {}
        
    Student* clone() const override {
          return new Student(*this);
    }
};

class Teacher : public virtual UniMember {

    Teacher(const Teacher& r) : Teacher{r.name(), r.dob()} {
        
    }  
    
public: 
    Teacher(const char* n, const Date& d)
        : UniMember(n, d) {}
        
    Teacher* clone() const override {
          return new Teacher(*this);
    }
};

class PhDStudent : public Teacher, public Student {

    PhDStudent(const PhDStudent& r) : PhDStudent{r.name(), r.dob()} {
        
    }  
    
public: 
    PhDStudent(const char* n, const Date& d)
        : UniMember(n, d), Teacher(nullptr, d), Student(nullptr, d) {}
        
    PhDStudent* clone() const override {
          return new PhDStudent(*this);
    }
};

void testNotInstantiable() {
    //UniMember uni{"Jo Doe", Date{1,1,1900}}; // => compilaiton error
}

void testStudent() {
    UniMember* student = new Student{"Popescu", Date{21, 3, 2000}};
    IMyObject* studentClone = student->clone();

    assert(0<strstr(typeid(*studentClone).name(), "Student"), "Wrong pointer type");
    assert(0==strcmp("Popescu", dynamic_cast<Student *>(studentClone)->name()), "Clone failed");
}

void testTeacher() {
    UniMember* teacher = new Teacher{"Popescu", Date{10, 10, 1977}};
    IMyObject* teacherClone = teacher->clone();

    assert(0<strstr(typeid(*teacherClone).name(), "Teacher"), "Wrong pointer type");
    assert(0==strcmp("Popescu", dynamic_cast<Teacher *>(teacherClone)->name()), "Clone failed");
}

void testPhDStudent() {
    UniMember* phd = new PhDStudent{"Popescu", Date{30, 8, 1992}};
    IMyObject* phdClone = phd->clone();

    assert(0<strstr(typeid(*phdClone).name(), "PhDStudent"), "Wrong pointer type");
    assert(0==strcmp("Popescu", dynamic_cast<PhDStudent *>(phdClone)->name()), "Clone failed");
}

int main()
{
    testNotInstantiable();
    testTeacher();
    testStudent();
    testPhDStudent();
    return 0;
}
