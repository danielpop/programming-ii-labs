#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <numeric>
#include <vector>

using namespace std;

class FreeStoreProxy {
public: 
    FreeStoreProxy();
    
    static FreeStoreProxy& instance();
    
    void* allocate(size_t size);
    
    void release(void* ptr) ;
    
    int getDanglingPointersCount() const;
    
    void freeAll();

private:
    static const int CAPACITY = 1024;
    static FreeStoreProxy _instance;
    void* entries[CAPACITY];
    void* const* position() const;
} ;

FreeStoreProxy FreeStoreProxy::_instance;

inline void* const* FreeStoreProxy::position() const {
    return std::find(entries, entries+CAPACITY, nullptr);
}
    
FreeStoreProxy::FreeStoreProxy() {
    //std::cout << "FreeStoreProxy::FreeStoreProxy" << std::endl;
    std::fill_n(entries, CAPACITY, nullptr); 
}

inline FreeStoreProxy& FreeStoreProxy::instance() {
    return _instance;
}   

void* FreeStoreProxy::allocate(size_t size) {
    void** pos = (void **)position();
    if (pos!=entries+CAPACITY) {
        void* ptr = malloc(size);
        *pos = ptr;
        //std::cout << "new " << size <<  " bytes @" << ptr << " cached @[" << (pos-entries) << "]" << std::endl;
        return ptr;
    }
    return nullptr;
}

void FreeStoreProxy::release(void* ptr) {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]==ptr) {
            //std::cout << "delete @" << ptr << std::endl;
            free(ptr);
            entries[i] = nullptr;
            break;
        }
    }
}

int FreeStoreProxy::getDanglingPointersCount() const {
    // int counter = 0;
    // std::for_each(entries, entries+CAPACITY, [&](void* p) { counter += (p!=nullptr); });
    // return counter;
    //return std::accumulate(entries, entries+CAPACITY, 0, [](int a, void* p) { return a + (p!=nullptr); });
    return CAPACITY - std::count(entries, entries+CAPACITY, nullptr);
}

void FreeStoreProxy::freeAll() {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]!=nullptr) {
            //std::cout << "delete @" << entries[i] << std::endl;
            free(entries[i]);
            entries[i] = nullptr;
        }
    }
}
        
void * operator new(size_t size) {
    return FreeStoreProxy::instance().allocate(size);
}

void operator delete(void *ptr) noexcept {
    FreeStoreProxy::instance().release(ptr);
}  


// Lab10TestSuite.h
class Lab10TestSuite {
    
public:
    Lab10TestSuite();

    int runTests();
    
private:
    typedef void (Lab10TestSuite::*Test)() const;
    
    std::vector<std::pair<const char*, Test>> tests;
    
    void registerTest(const char* description, Test fnTest);
    
    void assert(bool expression, const char* context, const char* message) const;
    
    // Actual tests declarations
    void test1() const;

    void test2() const;
    
    void test3() const;
} ; 

// Lab10TestSuite.cpp
Lab10TestSuite::Lab10TestSuite() {
    registerTest("happy path", &Lab10TestSuite::test1);
    registerTest("input/output exception", &Lab10TestSuite::test2);
    registerTest("file not found exception", &Lab10TestSuite::test3);
}

int Lab10TestSuite::runTests() {
    int failedTests = std::accumulate(tests.begin(), tests.end(), 0, [this](int counter, std::pair<const char*, Test> entry) { 
        std::cout << "Running test " << entry.first << "...\n";
        const int before = FreeStoreProxy::instance().getDanglingPointersCount();
        (this->*entry.second)();
        int after = FreeStoreProxy::instance().getDanglingPointersCount() - before;
        if (after!=0) {
            std::cout << "Assertion failed: Memory leak in test [" << entry.first << "]\n";
        }
        return counter + (after!=0);
    });
    
    std::cout << "Tests run: " << tests.size() << ", Passed: " << (tests.size()-failedTests) << ", Failed: " << failedTests << std::endl; 
    return 0;
}
    
inline void Lab10TestSuite::assert(bool expression, const char* context, const char* message) const {
    if (!expression) {
        std::cout << "Assertion failed in [" << context << "]: " << message << std::endl;
    }
}

inline void Lab10TestSuite::registerTest(const char* description, Test fnTest) {
    tests.push_back(std::pair<const char*, Test>{description, fnTest});
}

void Lab10TestSuite::test1 () const {
	try {
	    const char* fileName = "sample.txt";
        TextFile txtFile{fileName};
        txtFile.write("First line\n\r");
        txtFile.write("Second line\n\r");
        txtFile.close();
	}
	catch(FileNotFoundException& e1) {
	    assert(false, "test1", "FileNotFoundException should not be thrown.");
	}
	catch(FileWriteException& e2) {
         assert(false, "test1", "FileNotFoundException should not be thrown.");
	}    
}

void Lab10TestSuite::test2 () const {
	try {
	    const char* fileName = "c:\\folder-does-not-exist\\sample.txt";
        TextFile txtFile{fileName};
        assert(false, "test2", "shouldn't be here.");
	}
	catch(IOException& e) {
	    assert(strcmp("File not found c:\\folder-does-not-exist\\sample.txt", e.describe())==0, "test2", "FileNotFoundException describe issue.");
	}
}

void Lab10TestSuite::test3 () const {
	try {
	    const char* fileName = "c:\\folder-does-not-exist\\sample.txt";
        TextFile txtFile{fileName};
        assert(false, "test3", "shouldn't be here.");
	}
	catch(FileNotFoundException e) {
	    assert(strcmp("File not found c:\\folder-does-not-exist\\sample.txt", e.describe())==0, "test3", "FileNotFoundException describe issue.");
	}
}

int main() {
    return Lab10TestSuite{}.runTests();
} 
