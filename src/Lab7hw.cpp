#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>

using namespace std;

class List {
    // member (nested) class
    class Node;

    Node *top, *tail;

public:
    class Iterator {
    private:
        Node* ptr;
        
    public:
        Iterator(const List& l);
        
        bool isDone() const;
        void next();
        int current() const;
    };

    List();
    List(initializer_list<int> elements);
    List(const List& other);
    
    List& operator+=(int v);
    List& operator-=(int v);
    List& operator=(const List& l);
    
    Iterator iterator() const;
};

List::List(initializer_list<int> elements) 
    : List() {
    for( auto elem : elements ) {
        operator+=(elem);
    }
}

// Helper functions
ostream& operator<<(ostream& os, const List& l) {
}

bool operator==(const List& l1, const List& l2) {
    List::Iterator i1 = l1.iterator(), i2 = l2.iterator();
    while (!i1.isDone() && !i2.isDone()) {
        if (i1.current() != i2.current()) {
            return false;
        }        
        i1.next();
        i2.next();
    }

    return i1.isDone() && i2.isDone();
}

// FreeStoreProxy.h
class FreeStoreProxy {
public: 
    static FreeStoreProxy& instance();
    
    void* allocate(size_t size);
    
    void release(void* ptr) ;
    
    int getDanglingPointersCount() const;
    
    void freeAll();

private:
    static const int CAPACITY = 1024;
    static FreeStoreProxy _instance;
    int dbgLevel;
    void* entries[CAPACITY];
    
    FreeStoreProxy(int debugLevel=0);
    void* const* position() const;
    void debug(const char* msg) const;
} ;

// FreeStoreProxy.cpp
FreeStoreProxy FreeStoreProxy::_instance;

inline void* const* FreeStoreProxy::position() const {
    return std::find(entries, entries+CAPACITY, nullptr);
}
    
FreeStoreProxy::FreeStoreProxy(int debugLevel) : dbgLevel{debugLevel} {
    std::fill_n(entries, CAPACITY, nullptr); 
    debug("FreeStoreProxy::FreeStoreProxy");
}

inline FreeStoreProxy& FreeStoreProxy::instance() {
    return _instance;
}   

void* FreeStoreProxy::allocate(size_t size) {
    void** pos = (void **)position();
    if (pos!=entries+CAPACITY) {
        void* ptr = malloc(size);
        *pos = ptr;
        debug("allocate memory");
        return ptr;
    }
    return nullptr;
}

void FreeStoreProxy::release(void* ptr) {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]==ptr) {
            debug("free memory");
            free(ptr);
            entries[i] = nullptr;
            break;
        }
    }
}

int FreeStoreProxy::getDanglingPointersCount() const {
    // int counter = 0;
    // std::for_each(entries, entries+CAPACITY, [&](void* p) { counter += (p!=nullptr); });
    // return counter;
    //return std::accumulate(entries, entries+CAPACITY, 0, [](int a, void* p) { return a + (p!=nullptr); });
    return CAPACITY - std::count(entries, entries+CAPACITY, nullptr);
}

void FreeStoreProxy::freeAll() {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]!=nullptr) {
            debug("free memory");
            free(entries[i]);
            entries[i] = nullptr;
        }
    }
}

void FreeStoreProxy::debug(const char* msg) const {
    if (dbgLevel>0) {
        std::cout << "FreeStoreProxy - " << msg << std::endl;
    }
}
        
void * operator new(size_t size) {
    return FreeStoreProxy::instance().allocate(size);
}

void operator delete(void *ptr) noexcept {
    FreeStoreProxy::instance().release(ptr);
}


// Lab7TestSuite.h
class Lab7TestSuite {
    
public:
    Lab7TestSuite();

    int runTests();
    
private:
    typedef void (Lab7TestSuite::*Test)() const;
    
    std::vector<std::pair<const char*, Test>> tests;
    
    void registerTest(const char* description, Test fnTest);
    
    void assert(bool expression, const char* context, const char* message) const;
    
    // Actual tests declarations
    void testDefaultCtor() const;

    void testOperatorPlus() const;
    
    void testOperatorMinus() const;
    
    void testCopyCtor() const;
    
    void testOperatorAssignment() const;
    
    void testExpressionEvaluation() const;
    
    void testIterator() const;
    
    void testOpratorStreamOutput() const;
} ;


// Lab7TestSuite.cpp
Lab7TestSuite::Lab7TestSuite() {
    registerTest("default ctor", &Lab7TestSuite::testDefaultCtor);
    registerTest("operator +=", &Lab7TestSuite::testOperatorPlus);
    registerTest("operator -=", &Lab7TestSuite::testOperatorMinus);
    registerTest("copy ctor", &Lab7TestSuite::testCopyCtor);
    registerTest("opeator =", &Lab7TestSuite::testOperatorAssignment);
    registerTest("simple expression evaluation", &Lab7TestSuite::testExpressionEvaluation);
    registerTest("list iterator", &Lab7TestSuite::testIterator);
    registerTest("operator <<", &Lab7TestSuite::testOpratorStreamOutput);
}

int Lab7TestSuite::runTests() {
    int failedTests = std::accumulate(tests.begin(), tests.end(), 0, [this](int counter, std::pair<const char*, Test> entry) { 
        std::cout << "Running test " << entry.first << "...\n";
        const int before = FreeStoreProxy::instance().getDanglingPointersCount();
        (this->*entry.second)();
        int after = FreeStoreProxy::instance().getDanglingPointersCount() - before;
        if (after!=0) {
            std::cout << "Assertion failed: Memory leak in test [" << entry.first << "]\n";
        }
        return counter + (after!=0);
    });
    
    std::cout << "Tests run: " << tests.size() << ", Passed: " << (tests.size()-failedTests) << ", Failed: " << failedTests << std::endl; 
    return 0;
}
    
inline void Lab7TestSuite::assert(bool expression, const char* context, const char* message) const {
    if (!expression) {
        std::cout << "Assertion failed in [" << context << "]: " << message << std::endl;
    }
}

inline void Lab7TestSuite::registerTest(const char* description, Test fnTest) {
    tests.push_back(std::pair<const char*, Test>{description, fnTest});
}

void Lab7TestSuite::testDefaultCtor() const {
    List l;
    assert(List{}==l, "testDefaultCtor", "internal representation error");
}

void Lab7TestSuite::testOperatorPlus() const {
    List l;
    l+=1;
    l+=2;
    l+=3;
    
    assert(List{1, 2, 3}==l, "testOpratorPlus", "failed to add elements to list");
}

void Lab7TestSuite::testOperatorMinus() const {
    List l;
    l+=1;
    l+=2;
    l-=1;
    
    assert(List{2}==l, "testOpratorMinus", "failed to remove elements from list (1)");
    
    l-=2;
    assert(List{}==l, "testOpratorMinus", "failed to remove elements from list (2)");
}

void Lab7TestSuite::testCopyCtor() const {
    List l{1, 3, 5, 7}, l2 = l;
    
    assert(l==l2, "testCopyCtor", "internal representation error (1)");
    
    l-=3;
    assert(List{1, 3, 5, 7}==l2, "testCopyCtor", "internal representation error (2)");
    assert(List{1, 5, 7}==l, "testCopyCtor", "internal representation error (3)");
}

void Lab7TestSuite::testOperatorAssignment() const {
    List l{1, 2, 3, 4 ,5}, l2;
    l2 = l;
    
    assert(l==l2, "testOperatorAssignment", "assignment failed (1)");
    
    l2-=3;
    assert(List{1, 2, 4, 5}==l2, "testOperatorAssignment", "assignment failed (2)");
    assert(List{1, 2, 3, 4, 5}==l, "testOperatorAssignment", "assignment failed (3)");   
}

void Lab7TestSuite::testExpressionEvaluation() const {
    List l;
    l = (l+=1)-=2;
    
    assert(List{1}==l, "testExpressionEvaluation", "expression evaluation failed");
}

void Lab7TestSuite::testIterator() const {
    List l;

    for(List::Iterator i = l.iterator(); !i.isDone(); i.next()) {
        assert(false, "testIterator", "failed to iterate an empty list");
    }
    
    List l1{1};
    
    for(List::Iterator i = l1.iterator(); !i.isDone(); i.next()) {
        assert(1==i.current(), "testIterator", "failed to iterate 1-element list");
    }    
    
    List l2{2, 3};
    List::Iterator i = l2.iterator();
    assert(false==i.isDone(), "testIterator", "failed to iterate 2-element list (1)");
    assert(2==i.current(),    "testIterator", "failed to iterate 2-element list (2)");
    i.next();
    assert(false==i.isDone(), "testIterator", "failed to iterate 2-element list (3)");
    assert(3==i.current(),    "testIterator", "failed to iterate 2-element list (4)");
    i.next();
    assert(true==i.isDone(),  "testIterator", "failed to iterate 2-element list (5)");
}

void Lab7TestSuite::testOpratorStreamOutput() const {
    cout << List{1, 2, 3, 4, 5} << endl;
}


int main() {
    return Lab7TestSuite{}.runTests();
}
