#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>

class FileNotFoundException {
public:
	const char* describe() { return "File not found exc."; }
};

class FileWriteException {
public:
	const char* describe() { return "File write exc."; }
};

class File {
	FILE* handle;
	std::string name;	
	File(const char*);
public:
	static File open(const char* name);	
	void write(const char* data, int size);
	~File();
};

File::File(const char* n) : name{n}, handle(nullptr) {
}

File::~File() {
       if (handle!=nullptr) {
 	     fclose(handle);
       }
}

File File::open(const char* name) {
 	FILE* handle = fopen(name, "w+");
 	if(handle==NULL) {
      	      throw FileNotFoundException{};
       }
 	File f(name);
 	f.handle = handle;
 	return f;
}

int main() {
	try {
	        const char* fileName = "c:\\bdfkbdskfs\\sample.txt";
    	    const char* test = "Write test in my sample file.";
            std::cout << "Opening file " << fileName << "..." << std::endl;
    	    File f = File::open(fileName);
            std::cout << "Writing to file..." << std::endl;
    	    f.write(test, strlen(test));
            std::cout << "All done." << std::endl;    
	}
	catch(FileNotFoundException& e1) {
         std::cerr << e1.describe();
	}
	catch(FileWriteException& e2) {
         std::cerr << e2.describe();
	}
}
