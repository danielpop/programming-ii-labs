// Lab4 program
#include <iostream>
#include <string.h>

class Lab4Tests  {
    void assert(bool expression, const char* context, const char* message);
    
    void testDefaultCtorShouldInitializeWithNull();
    void testCtorShouldAllocateOwnMemory();
    void testCopyCtorShouldNotAlterSource();
    void testCopyCtorShouldDuplicateContent();
    void testCopyCtorShouldInitializeWithNullOnNullSource();
    void testDestructorShouldNotFailOnNullString();
    void testDestructorShouldShouldFreeOwnMemory();
    void testSetShouldDuplicateContent();
    void testSetShoultNotAlterSource();
    void testGetShouldReturnNull();
    void testChainedSet();
    void testIndexedGetShouldReturnAModifiableReference();

    public:
        void runAllTests();
};

void Lab4Tests::assert(bool expression, const char* context, const char* message) {
    if (!expression) {
        std::cout << "Assertion failed in [" << context << "]: " << message << std::endl;
    }
}

void Lab4Tests::testDefaultCtorShouldInitializeWithNull() {
    str s;
    assert(NULL==s.get(), "testDefaultCtorShouldInitializeWithNull", "internal representation error");
}

void Lab4Tests::testCtorShouldAllocateOwnMemory() {
    char* test = new char[strlen("Test")+1];
    strcpy(test, "Test");
    
    str s{test};
    assert(0==strcmp(test, s.get()), "testCtorShouldAllocateOwnMemory", "internal representation error");
    
    test[1] = 'E';
    assert(0==strcmp("TEst", test), "testCtorShouldAllocateOwnMemory", "shared memory error");
    assert(0==strcmp("Test", s.get()), "testCtorShouldAllocateOwnMemory", "shared memory error");
    
    delete [] test;
}

void Lab4Tests::testCopyCtorShouldNotAlterSource() {
    const char* test = "Test";
    str s1{test},  s2 = s1;
    assert(0==strcmp(test, s1.get()), "testCopyCtorShouldNotAlterSource", "internal representation error");
    assert(0==strcmp(test, s2.get()), "testCopyCtorShouldNotAlterSource", "internal representation error");
}

void Lab4Tests::testCopyCtorShouldDuplicateContent() {
    const char* test = "Test";
    str s1{test},  s2 = s1;
    assert(0==strcmp(test, s1.get()), "testCopyCtorShouldDuplicateContent", "internal representation error");
    assert(0==strcmp(test, s2.get()), "testCopyCtorShouldDuplicateContent", "internal representation error");

    s1.set(3, 'T');
    assert(0==strcmp("TesT", s1.get()), "testCopyCtorShouldDuplicateContent", "internal representation error");
    assert(0==strcmp(test, s2.get()), "testCopyCtorShouldDuplicateContent", "internal representation error");
}

void Lab4Tests::testCopyCtorShouldInitializeWithNullOnNullSource() {
    str s1, s2 = s1;
    assert(NULL==s1.get(), "testCopyCtorShouldInitializeWithNullOnNullSource", "internal representation error");
    assert(NULL==s2.get(), "testCopyCtorShouldInitializeWithNullOnNullSource", "internal representation error");
}


void Lab4Tests::testDestructorShouldNotFailOnNullString() {
    str* pstr = new str;
    delete pstr;
    assert(true, "testDestructorShouldNotFailOnNullString", "destructor failed on NULL content");
}

void Lab4Tests::testDestructorShouldShouldFreeOwnMemory() {
    const char* test = "Test";
    str s1{test};
    str* pstr = new str{s1};
    delete pstr;
    assert(0==strcmp(test, s1.get()), "testDestructorShouldShouldFreeOwnMemory", "memory deallocation failed");
}

void Lab4Tests::testSetShouldDuplicateContent() {
    char* test = new char[strlen("Test")+1];
    strcpy(test, "Test");
    str s1,  s2;
    assert(NULL==s1.get(), "testSetShouldDuplicateContent", "internal representation error");
    assert(NULL==s2.get(), "testSetShouldDuplicateContent", "internal representation error");

    s1.set(test);
    assert(0==strcmp("Test", test), "testSetShouldDuplicateContent", "internal representation error");
    assert(0==strcmp(test, s1.get()), "testSetShouldDuplicateContent", "internal representation error");
    
    test[3] = 'T';
    assert(0==strcmp("TesT", test), "testSetShouldDuplicateContent", "internal representation error");
    assert(0==strcmp("Test", s1.get()), "testSetShouldDuplicateContent", "internal representation error");
    delete test;
}

void Lab4Tests::testSetShoultNotAlterSource() {
    const char* test = "Test";
    str s1,  s2;
    assert(NULL==s1.get(), "testSetShoultNotAlterSource", "internal representation error");
    assert(NULL==s2.get(), "testSetShoultNotAlterSource", "internal representation error");

    s1.set(test);
    assert(0==strcmp("Test", test), "testSetShoultNotAlterSource", "internal representation error");
    assert(0==strcmp(test, s1.get()), "testSetShoultNotAlterSource", "internal representation error");
}

void Lab4Tests::testGetShouldReturnNull() {
    assert(NULL==str{}.get(), "testGetShouldReturnNull", "internal representation error");
}

void Lab4Tests::testChainedSet() {
    str greet = "Hello";
    assert(0==strcmp("Hello", greet.get()), "testChainedSet", "internal representation error");
    
    greet.set("Hi").set("Buna");
    assert(0==strcmp("Buna", greet.get()), "testChainedSet", "set failed");
}

void Lab4Tests::testIndexedGetShouldReturnAModifiableReference() {
    const char* test = "Test";
    str s1{test},  s2 = s1;
    assert(0==strcmp(test, s1.get()), "testIndexedGetShouldReturnAModifiableReference", "internal representation error");
    assert(0==strcmp(test, s2.get()), "testIndexedGetShouldReturnAModifiableReference", "internal representation error");

    s1.get(3)='T';
    assert(0==strcmp("TesT", s1.get()), "testIndexedGetShouldReturnAModifiableReference", "internal representation error");
    assert(0==strcmp(test, s2.get()), "testIndexedGetShouldReturnAModifiableReference", "internal representation error");
}

void Lab4Tests::runAllTests() {
    std::cout << "Running test testDefaultCtorShouldInitializeWithNull..." << std::endl;
    testDefaultCtorShouldInitializeWithNull();
    std::cout << "Running test testCtorShouldAllocateOwnMemory..." << std::endl;
    testCtorShouldAllocateOwnMemory();
    std::cout << "Running test testCopyCtorShouldNotAlterSource..." << std::endl;
    testCopyCtorShouldNotAlterSource();
    std::cout << "Running test testCopyCtorShouldDuplicateContent..." << std::endl;
    testCopyCtorShouldDuplicateContent();
    std::cout << "Running test testCopyCtorShouldInitializeWithNullOnNullSource..." << std::endl;
    testCopyCtorShouldInitializeWithNullOnNullSource();
    std::cout << "Running test testDestructorShouldNotFailOnNullString..." << std::endl;
    testDestructorShouldNotFailOnNullString();
    std::cout << "Running test testDestructorShouldShouldFreeOwnMemory..." << std::endl;
    testDestructorShouldShouldFreeOwnMemory();
    std::cout << "Running test testSetShouldDuplicateContent..." << std::endl;
    testSetShouldDuplicateContent();
    std::cout << "Running test testSetShoultNotAlterSource..." << std::endl;
    testSetShoultNotAlterSource();
    std::cout << "Running test testGetShouldReturnNull..." << std::endl;
    testGetShouldReturnNull();
    std::cout << "Running test testChainedSet..." << std::endl;
    testChainedSet();
    std::cout << "Running test testIndexedGetShouldReturnAModifiableReference..." << std::endl;
    testIndexedGetShouldReturnAModifiableReference();
}

int main() {
    Lab4Tests tests;
    tests.runAllTests();
    return 0;
}
