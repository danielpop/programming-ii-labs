class str {
public:
    str(const char* cStr = NULL) {
	// members' initialization is ommitted for brevity
        INSTANCES++;
    }
    
    str(const str &source) {
	// members' initialization is ommitted for brevity
        INSTANCES++;
    }
    
    static int countInstances() {
        return INSTANCES;
    }
    
private:
    static int INSTANCES;
    // other members
};

int str::INSTANCES = 0;

