#include <iostream>
#include <sstream>
#include <string.h>

using namespace std;

class str {
public:
    str(const char* str = nullptr);
    
    str(const str& source);

    // accessor function
    const char* get () const;
    
    // assignment
    str& operator=(const str& right);
    str& operator+=(const str& right);
    
    //mutator
    str& set(int pos, char c);

    ~str();
    
private:
    char* s;
    
    char* set(const char* ccp);
    static int length(const char* ccp);
};


str::str(const char* str) : s {set(str)} {}
    
str::str(const str &source) : str {source.s} {}
    
inline const char* str::get () const {
    return (const char*)s;
}
    
str& str::operator=(const str& right) {
    // self-assignemt
    if (this == &right) {
        return *this;
    }
        
    if (s!=nullptr) {
		delete [] s;
	}
		
    s = set(right.s);
        
	return *this;
}

str& str::operator+=(const str& right) {
    char* olds = s;
    
    s = new char[length(olds) + length(right.s) + 1];
    
    if (olds!=nullptr) {
        strcpy(s, olds);
    }
    
    if (right.s!=nullptr) {
        strcat(s, right.s);
    }
    
    if (olds!=nullptr) {
        delete [] olds;
    }
    
    return *this;
}
    
str::~str() { 
    if (s!=nullptr) {
		delete [] s;
	}
}
    
char* str::set(const char* ccp) {
    s = nullptr;
	
    if (ccp!=nullptr) {
        s = new char [strlen(ccp) + 1];
        strcpy(s, ccp);
    }
        
	return s;
}

str& str::set(int pos, char c) {
    s[pos] = c;
    return *this;
}


int str::length(const char* ccp) {
    return ccp==nullptr ? 0 : strlen(ccp);
}

ostream& operator<<(ostream& os, const str& s) {
    os << s.get();
    return os;
}

str operator+(const str& left, const str& right) {
    return str{left}+=(right);
}

class Shape {
private:
    int id;
    char _rep[128];
    std::stringstream stream;
    
    static int ID;
    static int generateId();
    
protected:
    str repr() const;
    ostream& reprStream() ;
    
public:
    void display() const;
    Shape();
};

class Point : private Shape {
    int x, y;
public:
    str toString() ;
};

class Line : protected Shape {
    Point points[2];
public:
    str toString() ;
};

class Circle : public Shape {
    int radius;
    Point center;
public:
    str toString() ;
};

int Shape::ID = 1000;

int Shape::generateId() {
    return Shape::ID++;
}

Shape::Shape() : id{Shape::generateId()} {
    stream.rdbuf()->pubsetbuf(_rep, sizeof(_rep));
}

str Shape::repr() const {
    return str{_rep};
}

inline ostream& Shape::reprStream() {
    stream.seekp(0);
    return stream;
}

void Shape::display() const {
    std::cout << "ID: " << id << " " << repr() << std::endl;
}

str Point::toString() {
    //id = 100; // id not accessible because it is private in the base class Shape
    //Note that repr() and reprStream() are accessible because they are protected in the base class Shape
    reprStream() << "{type: Point, x: " << x << ", y: " << y << "}" << (char)0;
    return repr();
}

str Line::toString() {
    reprStream() << "{type: Line, from: " << points[0].toString() << ", to: " << points[1].toString() << "}" << (char)0;
    return repr();
}

str Circle::toString() {
    reprStream() << "{type: Circle, center: " << center.toString() << ", radius: " << radius << "}" << (char)0;
    return repr();
}

int main()
{
    Shape s;
    Point p;
    Line l;
    Circle c;

    s.display();
    
    p.toString();
    //p.display();  //This is not accessible because Shape is a private base of Point
    cout << "From main: " << p.toString() << std::endl;
    
    l.toString();
    //l.display(); //This is not  accessible because Shape is a protected base of Point
    cout << "From main: " << l.toString() << std::endl;
    
    c.toString();
    c.display();
    
    return 0;
}
