#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>

using namespace std;

class FreeStoreProxy {
public: 
    FreeStoreProxy();
    
    static FreeStoreProxy& instance();
    
    void* allocate(size_t size);
    
    void release(void* ptr) ;
    
    int getDanglingPointersCount() const;
    
    void freeAll();

private:
    static const int CAPACITY = 1024;
    static FreeStoreProxy _instance;
    void* entries[CAPACITY];
    void* const* position() const;
} ;

FreeStoreProxy FreeStoreProxy::_instance;

inline void* const* FreeStoreProxy::position() const {
    return std::find(entries, entries+CAPACITY, nullptr);
}
    
FreeStoreProxy::FreeStoreProxy() {
    //std::cout << "FreeStoreProxy::FreeStoreProxy" << std::endl;
    std::fill_n(entries, CAPACITY, nullptr); 
}

inline FreeStoreProxy& FreeStoreProxy::instance() {
    return _instance;
}   

void* FreeStoreProxy::allocate(size_t size) {
    void** pos = (void **)position();
    if (pos!=entries+CAPACITY) {
        void* ptr = malloc(size);
        *pos = ptr;
        //std::cout << "new " << size <<  " bytes @" << ptr << " cached @[" << (pos-entries) << "]" << std::endl;
        return ptr;
    }
    return nullptr;
}

void FreeStoreProxy::release(void* ptr) {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]==ptr) {
            //std::cout << "delete @" << ptr << std::endl;
            free(ptr);
            entries[i] = nullptr;
            break;
        }
    }
}

int FreeStoreProxy::getDanglingPointersCount() const {
    // int counter = 0;
    // std::for_each(entries, entries+CAPACITY, [&](void* p) { counter += (p!=nullptr); });
    // return counter;
    //return std::accumulate(entries, entries+CAPACITY, 0, [](int a, void* p) { return a + (p!=nullptr); });
    return CAPACITY - std::count(entries, entries+CAPACITY, nullptr);
}

void FreeStoreProxy::freeAll() {
    for(int i=0; i<CAPACITY; i++) {
        if (entries[i]!=nullptr) {
            //std::cout << "delete @" << entries[i] << std::endl;
            free(entries[i]);
            entries[i] = nullptr;
        }
    }
}
        
void * operator new(size_t size) {
    return FreeStoreProxy::instance().allocate(size);
}

void operator delete(void *ptr) noexcept {
    FreeStoreProxy::instance().release(ptr);
}  


// Lab11TestSuite.h
class Lab11TestSuite {
    
public:
    Lab11TestSuite();

    int runTests();
    
private:
    typedef void (Lab11TestSuite::*Test)() const;
    
    std::vector<std::pair<const char*, Test>> tests;
    
    void registerTest(const char* description, Test fnTest);
    
    void assert(bool expression, const char* context, const char* message) const;
    
    // Actual tests declarations
    void testEmptyStack() const;

    void testStackPushPop() const;
} ; 

// Lab11TestSuite.cpp
Lab11TestSuite::Lab11TestSuite() {
    registerTest("empty stack", &Lab11TestSuite::testEmptyStack);
    registerTest("stack push/pop", &Lab11TestSuite::testStackPushPop);
}

int Lab11TestSuite::runTests() {
    int failedTests = std::accumulate(tests.begin(), tests.end(), 0, [this](int counter, std::pair<const char*, Test> entry) { 
        std::cout << "Running test " << entry.first << "...\n";
        const int before = FreeStoreProxy::instance().getDanglingPointersCount();
        (this->*entry.second)();
        int after = FreeStoreProxy::instance().getDanglingPointersCount() - before;
        if (after!=0) {
            std::cout << "Assertion failed: Memory leak in test [" << entry.first << "]\n";
        }
        return counter + (after!=0);
    });
    
    std::cout << "Tests run: " << tests.size() << ", Passed: " << (tests.size()-failedTests) << ", Failed: " << failedTests << std::endl; 
    return 0;
}
    
inline void Lab11TestSuite::assert(bool expression, const char* context, const char* message) const {
    if (!expression) {
        std::cout << "Assertion failed in [" << context << "]: " << message << std::endl;
    }
}

inline void Lab11TestSuite::registerTest(const char* description, Test fnTest) {
    tests.push_back(std::pair<const char*, Test>{description, fnTest});
}

void Lab11TestSuite::testEmptyStack() const {
    try {
      MyStack<float> s;
      assert(s.getSize()==0, "empty Stack", "Bad size");
      s.pop();

      assert(false, "empty Stack", "Should not be here!");
    }
    catch(EmptyStackException& e)  {
      assert(true, "emptyStack", "");
    }
}

void Lab11TestSuite::testStackPushPop() const {
    try {
      MyStack<int> s;
      for (int i=0; i<10; i++) {
        s.push(i);
      }
    
      assert(s.getSize()==10, "stack push/pop", "Bad size");
      assert(s.pop()==9, "stack push/pop", "Bad pop()");
      assert(s.pop()==8, "stack push/pop", "Bad pop()");
    }
    catch(EmptyStackException& e)  {
      assert(false, "stack push/pop", "Should not be here!");
    }
}


int main() {
    return Lab11TestSuite{}.runTests();
} 
